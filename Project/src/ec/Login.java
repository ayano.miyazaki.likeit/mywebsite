package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログイン画面にリダイレクト
		RequestDispatcher dispatchar = request.getRequestDispatcher(EcHelper.LOGIN_PAGE);
		dispatchar.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		//リクエストパラーメタの入力項目(ログインID、パスワード)を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

//		//ハッシュを生成したい元の文字列
//		String source = password;
//		//ハッシュ生成前にバイト配列に置き換える際のCharset
//		Charset charset = StandardCharsets.UTF_8;
//		//ハッシュアルゴリズム
//		String algorithm = "MD5";
//		//ハッシュ生成処理
//		byte[] bytes = null;
//		try {
//			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
//		String result = DatatypeConverter.printHexBinary(bytes);
//		//標準出力
//		System.out.println(result);

		UserDao userDao = new UserDao();
		UserBeans ub = userDao.findUserIdByLoginInfo(loginId, password);

		//テーブルにユーザー情報がない場合の処理
		if (ub == null) {
			request.setAttribute("LoginErrMsg", "ログインに失敗しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher(EcHelper.LOGIN_PAGE);
			dispatcher.forward(request, response);
		}

		//テーブルにデータがある場合の処理
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", ub);
		response.sendRedirect("StoreTop");
	}

}
