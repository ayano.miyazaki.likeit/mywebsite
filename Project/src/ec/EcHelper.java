package ec;

public class EcHelper {

	//ストアトップ
	static final String STORE_TOP_PAGE = "/WEB-INF/jsp/storeTop.jsp";
	//ログイン
	static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
	//ログアウト
	static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";
	//ログアウト完了
	static final String LOGOUT_RESULT_PAGE = "/WEB-INF/jsp/logoutResult.jsp";
	//新規登録
	static final String SIGN_UP_PAGE = "/WEB-INF/jsp/signUp.jsp";
	//新規登録確認
	static final String SIGN_UP_CONFITM_PAGE = "/WEB-INF/jsp/signUpConfirm.jsp";
	//新規登録完了
	static final String SIGN_UP_RESULT = "/WEB-INF/jsp/signUpResult.jsp";
	//ユーザー情報参照
	static final String USER_DETAIL = "/WEB-INF/jsp/userDetail.jsp";
	//ユーザー情報更新
	static final String USER_UPDATE = "/WEB-INF/jsp/userUpadate.jsp";
	//ユーザー情報更新確認
	static final String USER_UPDATE_CONFIRM = "/WEB-INF/jsp/userUpdateConfirm.jsp";
	//ユーザー情報更新完了
	static final String USER_UPDATE_RESULT = "/WEB-INF/jsp/userUpdateResult.jsp";
	//購入履歴
	static final String BUY_HISTORY = "/WEB-INF/jsp/buyHistory.jsp";
	//購入履歴詳細
	static final String BUY_HISTORY_DETAIL = "/WEB-INF/jsp/buyHistoryDetail.jsp";
	//ショッピングカート
	static final String SHOPPONG_CART = "/WEB-INF/jsp/shoppingCart.jsp";
	//購入確認
	static final String BUY_CONFIRM = "/WEB-INF/jsp/buyConfirm.jsp";
	//購入完了
	static final String BUY_RESULT = "/WEB-INF/jsp/buyResult.jsp";
	//商品一覧
	static final String ITEM_LIST = "/WEB-INF/jsp/itemList.jsp";
	//商品登録
	static final String ITEM_CREATE = "/WEB-INF/jsp/itemCreate.jsp";
	//商品登録確認
	static final String ITEM_CREATE_CONFIRM = "/WEB-INF/jsp/itemCreateConfirm.jsp";
	//商品登録完了
	static final String ITEM_CREATE_RESULT = "/WEB-INF/jsp/itemCreateResult.jsp";
	//商品更新
	static final String ITEM_UPDATE = "/WEB-INF/jsp/itemUpdate.jsp";
	//商品更新確認
	static final String ITEM_UPDATE_CONFIRM = "/WEB-INF/jsp/itemUpdateConfirm.jsp";
	//商品更新完了
	static final String ITEM_UPDATE_RESULT = "/WEB-INF/jsp/itemUpdateResult.jsp";
	//商品削除確認
	static final String ITEM_DELETE_CONFIRM = "/WEB-INF/jsp/itemDeleteConfirm.jsp";
	//商品削除完了
	static final String ITEM_DELETE_RESULT = "/WEB-INF/jsp/itemDeleteResult.jsp";

}