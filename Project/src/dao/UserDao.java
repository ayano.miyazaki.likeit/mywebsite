package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.UserBeans;

public class UserDao {

	//ログインIDとパスワードからユーザーIDの取得
	public static UserBeans findUserIdByLoginInfo(String loginId, String password) {

		Connection con = null;
		PreparedStatement st = null;
		UserBeans ub = new UserBeans();

		try {
			//DB接続
			con = DBManager.getConnection();

			//SQL文に値をセットし実行
			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ? AND password = ?");
			st.setString(1, loginId);
			st.setString(2, password);
			ResultSet rs = st.executeQuery();

			//ユーザーIDを返す
			while(rs.next()) {
				ub.setId(rs.getInt("id"));
				System.out.println("ログイン成功");
				break;
			}

			System.out.println("findUserIdByLoginInfoが完了しました");

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//DB切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return ub;
	}
}