<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

    <div class="headline">
      <a>商品一覧</a>
        </div>


<form class="form">
       <div class="form-group">
    <label for="exampleInputPassword1">商品ID</label>
    <input type="text" class="form-control" value="r0001">
  </div>

       <div class="form-group">
    <label for="exampleInputPassword1">商品分類</label>
    <p><input type="radio" name="riyu" value="1" checked="checked">ラーメン
<input type="radio" name="riyu" value="2">つけ麺
<input type="radio" name="riyu" value="3">まぜそば</p>
  </div>

       <div class="form-group">
    <label for="exampleInputPassword1">商品名</label>
    <input type="text" min="1" class="form-control">
  </div>




  <div class="button">
     <button type="button" class="submitBtn">検索</button>
       <button class=backBtn style="">商品を新しく登録</button>

</div>
 </form>
<br>
<table align="center" class="table table-striped">
  <thead>
    <tr>
      <th scope="col">商品ID</th>
      <th scope="col">商品分類</th>
      <th scope="col">商品名</th>
      <th scope="col">価格</th>
      <th scope="col">更新日時</th>
      <th scope="col">イメージ(カーソルを合わせて表示)</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">id0001</th>
      <td>ラーメン</td>
      <td>醤油ラーメン</td>
        <td>2300円</td>
    <td>2020/09/27</td>
        <td><a href="#" class="preview">表示<span><img src="/image/ramenSample.png" border="0" width="200" height="200"></span></a></td>
      <td>
		<button type="button" class="btn btn-success">更新</button>
		<button type="button" class="btn btn-danger">削除</button>
	  </td>
    </tr>
    <tr>
      <th scope="row">id0002</th>
     <td>ラーメン</td>
      <td>味噌ラーメン</td>
        <td>500円</td>
    <td>2020/09/27</td>
           <td><a href="#" class="preview">表示<span><img src="/image/ramenSample.png" border="0" width="200" height="200"></span></a></td>
 	   <td>
		<button type="button" class="btn btn-success">更新</button>
		<button type="button" class="btn btn-danger">削除</button>
	  </td>
    </tr>
  </tbody>
</table>

</body>
</html>