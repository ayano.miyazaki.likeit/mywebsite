<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

    <h1 class="headline">
      <a>購入確定</a>
    </h1>

    <br>

    <table align="center" class="table table-striped">
    <thead>
    <tr>
        <th> 商品名 </th>
        <th> 数量 </th>
        <th> 価格 </th>
        <th></th>
        </tr>
        </thead>
    <tbody>
        <tr>
        <td>サンプル商品1</td>
        <td>n</td>
        <td>77777円</td>
        <td><button type="submit">削除</button></td>
        </tr>
               <tr>
        <td>サンプル商品2</td>
        <td>m</td>
        <td>11111円</td>
        <td><button type="submit">削除</button></td>
        </tr>

               <tr>
        <td>配送料金</td>
        <td></td>
        <td>777円</td>
        <td></td>
        </tr>
        </tbody>
    </table>
                     <div class="form-group">
                               <div>注文が確定しました。</div><br>
    <label for="exampleInputPassword1">注文番号</label>
           <div>01344588374383839</div><br>

                           <label for="exampleInputPassword1">お届け予定日</label>
           <div>2020年7月14日</div><br>

  </div>
<div class="button">
                <button type="submit" class="submitBtn">ユーザー情報へ</button><br>
                <button type="submit" class="backBtn">トップへ戻る</button>
    </div>
</body>

</html>