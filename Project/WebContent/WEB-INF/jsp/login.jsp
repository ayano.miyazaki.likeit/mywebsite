<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">


<head>
<meta charset="utf-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/style.css">
</head>

<body>


	<form action="Login" method="post">
		<div class="form">
			<div class="headline">
				<a>ログイン画面</a>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">ログインID</label> <input type="text"
					class="form-control" name="loginId">
			</div>

			<div class="form-group">
				<label for="exampleInputPassword1">パスワード</label> <input
					type="password" class="form-control" name="password">
			</div>

			<div class="button">
				<button type="submit" class="submitBtn">ログイン</button>
				<button type="submit" class="backBtn">新規登録</button>
			</div>

		</div>
	</form>
</body>

</html>