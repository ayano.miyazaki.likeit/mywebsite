<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
     <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

<div class="container-fluid">

    <h1 class="headline">
      <a>ストアトップ</a>
    </h1>

    <div class="itemList">

    <div class="subHead"><i class="fa fa-cutlery" aria-hidden="true"></i>ラーメン</div>
    <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>醤油ラーメン</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>

   <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>塩ラーメン</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>

     <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>味噌ラーメン</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>



   <div class="subHead"><i class="fa fa-cutlery" aria-hidden="true"></i>つけ麺</div>
   <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>醤油つけ麺</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>

   <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>塩つけ麺</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>

     <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>味噌つけ麺</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>
    <br>

    <div class="subHead"><i class="fa fa-cutlery" aria-hidden="true"></i>まぜそば</div>
    <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>醤油まぜそば</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>

   <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>塩まぜそば</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>

     <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>味噌まぜそば</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>
<br>

      <div class="subHead"><i class="fa fa-cutlery" aria-hidden="true"></i>その他</div>
   <div class="item">
    <div class="ramenPicture">
    <img src="image/ramenSample.png" width="200px" height="200px">
    </div>
    <p>特製チャーシュー丼</p>
    <p>価格 : 750円</p>

     数量 : <input type="number" min="1" value="0"><br><br>
    <button type="submit">カートに入れる</button>
    </div>
</div>


<br>

</body>

</html>