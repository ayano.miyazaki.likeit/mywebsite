<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

    <div class="headline">
      <a>商品情報更新</a>
        </div>

<form class="form">
       <div class="form-group">
    <label for="exampleInputPassword1">商品ID</label>
    <input type="text" class="form-control" value="r0001">
  </div>

       <div class="form-group">
    <label for="exampleInputPassword1">商品分類</label>
    <p><input type="radio" name="riyu" value="1" checked="checked">ラーメン
<input type="radio" name="riyu" value="2">つけ麺
<input type="radio" name="riyu" value="3">まぜそば</p>
  </div>

       <div class="form-group">
    <label for="exampleInputPassword1">商品名</label>
    <input type="text" class="form-control" value="ラーメン">
  </div>

       <div class="form-group">
    <label for="exampleInputPassword1">商品価格(円)</label>
    <input type="number" class="form-control" value=750>
  </div>


      <div class="form-group">
    <label for="exampleInputPassword1">表示画像</label>
   <input type="file">
  </div>

  <div class="button">
     <button type="button" class="submitBtn">登録</button>
     <button type="button" class="backBtn">戻る</button>
</div>
 </form>

</body>
</html>